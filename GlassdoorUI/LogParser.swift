import Cocoa

struct LogParser {
    let decoder = JSONDecoder()
    let fileManager = FileManager.default
    let workspace = NSWorkspace.shared

    func parse(_ logString: String) -> LogEntry? {
        if let data = logString.data(using: .utf8) {
            do {
                return try decoder.decode(LogEntry.self, from: data)
            } catch {
                print("Failed to decode JSON: \(error.localizedDescription)")
            }
        }

        return nil
    }

    func parserFile(_ path: String) -> [LogEntry] {
        var logEntries = [LogEntry]()
        var pathCopy = path
        pathCopy.trimPrefix("file://")
        let exists = fileManager.fileExists(atPath: pathCopy)
        if !exists {
            print("File not found at path: \(path)")
            return logEntries
        }

        do {
            // Read the file line by line
            let fileContents = try String(contentsOfFile: pathCopy, encoding: .utf8)
            let lines = fileContents.components(separatedBy: .newlines)

            // Parse each line
            for line in lines {
                if let logEntry = parse(line) {
                    logEntries.append(logEntry)
                }
            }
        } catch {
            print("Error reading file: \(error)")
        }

        return logEntries
    }
}
