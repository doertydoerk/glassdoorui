import SwiftUI

@main
struct GlassdoorUIApp: App {
    let logService = LogsService()
    var body: some Scene {
        WindowGroup {
            ContentView(logsService: logService, sideBarVisibility: .automatic)
        }
    }
}
