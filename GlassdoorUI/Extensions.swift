import Foundation

public extension String {
    func removeSuffix(_ suffix: String) -> String {
        guard hasSuffix(suffix) else { return self }
        return String(dropLast(suffix.count))
    }

    func convertToArray() -> [String] {
        let replacedString = replacingOccurrences(of: ", ", with: ",")
        return replacedString.components(separatedBy: ",")
    }
}
