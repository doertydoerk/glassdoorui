import Foundation

enum LogLevel: String {
    case debug = "DEBUG"
    case info = "INFO"
    case warn = "WARN"
    case error = "ERROR"
}

struct LogEntry: Codable, Equatable {
    let ts: String
    let L: String
    let C: String
    let M: String
    var tags: [String]

    enum CodingKeys: String, CodingKey {
        case ts
        case L
        case C
        case M
        case tags
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        ts = try container.decode(String.self, forKey: .ts)
        L = try container.decode(String.self, forKey: .L)
        C = try container.decode(String.self, forKey: .C)
        M = try container.decode(String.self, forKey: .M)
        tags = try container.decode([String].self, forKey: .tags)
    }

    static func == (lhs: LogEntry, rhs: LogEntry) -> Bool {
        lhs.id == rhs.id
        // If there are other properties to compare, add them here...
    }
}

extension LogEntry: Identifiable {
    var id: String { ts } // Or any other property or combination that is unique
}
