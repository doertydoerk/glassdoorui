import Foundation
import Starscream

class WebSocketsClient {
    private var socket: WebSocket!
    let host: String
    let heartbeatInterval: TimeInterval = 4
    var isConnected = false
    var heartbeatTimer: Timer?

    init(host: String, delegate: WebSocketDelegate) {
        self.host = host
        initWebSockets(delegate: delegate)
    }

    func initWebSockets(delegate: WebSocketDelegate) {
        print("init with host: \(host)")

        var request = URLRequest(url: URL(string: "ws://\(host):3000/ws/logs")!)
        request.timeoutInterval = 5
        socket = WebSocket(request: request)
        if socket == nil {
            print("new socket is nil")
        }

        socket.delegate = delegate
        connect()
    }

    func connect() {
        socket.connect()
    }

    func disconnect() {
        socket?.disconnect()
    }

    func startHeartbeat() {
        heartbeatTimer = Timer.scheduledTimer(withTimeInterval: heartbeatInterval, repeats: true, block: { [weak self] _ in
            self?.socket.write(ping: Data())
        })
    }

    func stopHeartbeat() {
        heartbeatTimer?.invalidate()
        heartbeatTimer = nil
    }
}
