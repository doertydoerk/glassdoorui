import Foundation
import Starscream

class LogsService: ObservableObject {
    private let parser = LogParser()
    private let backgroundQueue = DispatchQueue.global(qos: .default)
    private static let hostKey = "hostKey"
    var wsClient: WebSocketsClient? {
        didSet {
            DispatchQueue.main.async {
                if self.wsClient != nil { self.networkError = true } else { self.networkError = false }
            }
        }
    }

    @Published var logs: [LogEntry] = []
    @Published var networkError = false
    @Published var isConnected = false

    // logSource may be a device host name or the path to a local log file
    @Published var logSource: String {
        didSet {
            if isLogFileURL(source: logSource) {
                // stop trying to connect to host
                wsClient?.stopHeartbeat()
                wsClient?.disconnect()
                wsClient = nil
                logs.removeAll()

                // parse log file
                logs = parser.parserFile(logSource)
            }

            setHost(host: logSource)
        }
    }

    func isLogFileURL(source: String) -> Bool {
        source.hasPrefix("file://") && source.hasSuffix(".log")
    }

    init() {
        logSource = LogsService.getHost()
        newSocketConnection(host: logSource)
    }

    func setHost(host: String) {
        if !matchesHostnamePattern(host) { return }

        handlePlayPause(isPlay: false)
        logs = []
        networkError = false

        handlePlayPause(isPlay: true)
        UserDefaults().setValue(host, forKey: LogsService.hostKey)
        UserDefaults().synchronize()
    }

    static func getHost() -> String {
        UserDefaults().string(forKey: LogsService.hostKey) ?? ""
    }

    func newSocketConnection(host: String) {
        print("trying to connect \(host)....")
        if host == "" { return }

        if !matchesHostnamePattern(host) {
            setHost(host: "")
            return
        }

        wsClient = WebSocketsClient(host: host, delegate: self as WebSocketDelegate)

        DispatchQueue.global(qos: .userInitiated).async {
            sleep(1)
            if self.isConnected {
                return
            }

            if self.isLogFileURL(source: self.logSource) {
                return
            }

            self.newSocketConnection(host: host)
        }
    }

    func handlePlayPause(isPlay: Bool) {
        if isPlay {
            print("play")
            newSocketConnection(host: logSource)
            return
        }

        wsClient?.disconnect()
        wsClient = nil
        isConnected = false
    }

    func matchesHostnamePattern(_ string: String) -> Bool {
        if string == "localhost" {
            return true
        }

        let pattern = "^fxt\\d{7}\\.local$"
        let regex = try? NSRegularExpression(pattern: pattern)
        let range = NSRange(location: 0, length: string.utf8.count)
        return regex?.firstMatch(in: string, options: [], range: range) != nil
    }
}

extension LogsService: WebSocketDelegate {
    func didReceive(event: Starscream.WebSocketEvent, client _: any Starscream.WebSocketClient) {
        switch event {
        case let .connected(headers):
            isConnected = true
            networkError = false
            wsClient?.startHeartbeat()
            print("webSocket is connected. Headers: \(headers)")
        case let .disconnected(reason, code):
            wsClient?.stopHeartbeat()
            isConnected = false
            networkError = true
            print("webSocket disconnected with reason: \(reason) Code: \(code)")
        case let .text(json):
            guard let log = parser.parse(json) else { return }
            logs.append(log)
        case let .binary(data):
            print("received data: \(data.count)")
        case let .error(error):
            print("networkError: \(error?.localizedDescription ?? "Unknown error")")
            networkError = true
            if logSource != "" {
                wsClient = nil
                isConnected = false
                newSocketConnection(host: logSource)
            }
        default:
            break
        }
    }
}

class MockLogsService: LogsService {
    override init() {
        super.init()
        logSource = "localhost"

        let json = """
        {
            "ts": "2024-04-11T15:51:34.823+0200",
            "L": "DEBUG",
            "C": "application/temperature_monitor.go:224",
            "M": "Battery status for MAIN_BATTERY: not charging, battery in place, time since removal: 0s, charging state: Medium, level: 94.1, voltage: 7.0 V, current: 0.98 A, power: 6.87 W, error: n/a, temperature: 45.2 °C, OK",
            "tags": ["battery", "service"]
        }
        """.data(using: .utf8)!
        let decoder = JSONDecoder()
        do {
            let fakeLog = try decoder.decode(LogEntry.self, from: json)
            logs = Array(repeating: fakeLog, count: 2)
        } catch {
            print("Error decoding LogEntry: \(error)")
        }
    }
}
