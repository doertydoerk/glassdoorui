import SwiftUI

struct ContentView: View {
    @ObservedObject var logsService: LogsService

    @State private var selectedOption1 = 0
    @State private var selectedOption2 = 0
    @State private var inputHost = ""
    @State private var inputKeywords = ""
    @State private var keywords: [String] = []
    @State private var isPlaying = false
    @State private var scrollDown = false
    @State private var importClicked = false
    @State private var filterDebugSelected = true
    @State private var filterInfoSelected = true
    @State private var filterWarnSelected = true
    @State private var filterErrorSelected = true
    @State var sideBarVisibility: NavigationSplitViewVisibility = .doubleColumn

    var body: some View {
        NavigationSplitView(columnVisibility: $sideBarVisibility) {
            sidebarView.frame(minWidth: 170)
        } detail: {
            mainView.frame(minWidth: 1000, maxWidth: .infinity)
        }
        .toolbar {
            ToolbarItem(placement: .principal) {
                HStack {
                    ZStack(alignment: .trailing) {
                        TextField("highlight keywords", text: $inputKeywords, onCommit: {
                            keywords = inputKeywords.convertToArray()
                        })
                        .textFieldStyle(.roundedBorder)
                        .frame(width: 300)
                        .padding(.vertical)

                        if !inputKeywords.isEmpty {
                            Button(action: {
                                inputKeywords = ""
                                keywords = []
                            }) {
                                Image(systemName: "multiply.circle.fill")
                                    .foregroundColor(.secondary)
                                    .padding(.trailing)
                            }
                        }
                    }

                    Button(action: {
                        isPlaying.toggle()
                        logsService.handlePlayPause(isPlay: isPlaying)
                    }) {
                        Image(systemName: logsService.networkError ? "play.slash.fill" : (logsService.isConnected ? "pause.fill" : "play.fill"))
                            .foregroundColor(.accentColor)
                    }.help(Text("start/stop getting logs"))

                    Button(action: {
                        scrollDown.toggle()
                    }) {
                        Image(systemName: scrollDown ? "arrow.down.to.line.compact" : "arrow.down.to.line.compact")
                            .foregroundColor(scrollDown ? Color(.accent) : Color(.gray))
                    }.help(Text("keep log to the last entry"))

                    Button(action: {
                        logsService.logs = []
                    }) {
                        Image(systemName: "xmark")
                            .foregroundColor(Color(.accent))
                    }.help(Text("clear the log history"))
                }
            }
        }
        .navigationTitle("Glassdoor")
        .frame(minWidth: 1200, minHeight: 600, alignment: .topLeading)
    }

    var sidebarView: some View {
        VStack {
            List {
                Section(header: Text("Host / Log File")) {
                    HStack {
                        TextField("host or IP", text: $inputHost, onCommit: {
                            logsService.logSource = inputHost
                        })
                        .foregroundColor(logsService.networkError ? .red : .primary)
                        .fontWeight(logsService.networkError ? .medium : .regular)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                        .onAppear(perform: {
                            inputHost = logsService.logSource
                        })

                        Button(action: {
                            importClicked.toggle()
                            showOpenPanel()

                        }) {
                            Image(systemName: "square.and.arrow.down")
                                .foregroundColor(.accent)
                        }
                        .buttonStyle(PlainButtonStyle())
                        .help(Text("import log file"))
                    }
                }

                Section(header: Text("Log Levels")) {
                    Toggle(isOn: $filterDebugSelected) {
                        Text("Debug")
                    }
                    Toggle(isOn: $filterInfoSelected) {
                        Text("Info")
                    }
                    Toggle(isOn: $filterWarnSelected) {
                        Text("Warn")
                    }
                    Toggle(isOn: $filterErrorSelected) {
                        Text("Error")
                    }
                }
            }
        }
    }

    var mainView: some View {
        VStack {
            ScrollViewReader { scrollView in
                List {
                    // create a subset so the altenating background colouring keeps working when filters kick in
                    let filteredLogs = logsService.logs.filter { !shouldSkipLog(logLevel: $0.L) }

                    ForEach(filteredLogs.indices, id: \.self) { index in
                        let log = filteredLogs[index]
                        TableEntry(log: log, keywords: keywords)
                            .listRowSeparator(.hidden)
                            .listRowBackground(index % 2 == 0 ? Color.gray.opacity(0.1) : Color.clear)
                            .padding(.vertical, 2)
                    }
                    .onReceive(logsService.$logs) { _ in
                        withAnimation {
                            if scrollDown {
                                let lastIndex = logsService.logs.count - 1
                                if lastIndex >= 0 {
                                    scrollView.scrollTo(lastIndex, anchor: .bottom)
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    func showOpenPanel() {
        let panel = NSOpenPanel()
        panel.allowsMultipleSelection = false
        panel.canChooseDirectories = false

        let response = panel.runModal()
        if response == .OK {
            if let hostURL = panel.url {
                inputHost = hostURL.lastPathComponent
                logsService.logSource = hostURL.absoluteString
            }
        }

        // Reset the button color after action is performed
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            importClicked.toggle()
        }
    }

    func shouldSkipLog(logLevel: String) -> Bool {
        switch logLevel {
        case LogLevel.debug.rawValue:
            return !filterDebugSelected
        case LogLevel.info.rawValue:
            return !filterInfoSelected
        case LogLevel.warn.rawValue:
            return !filterWarnSelected
        case LogLevel.error.rawValue:
            return !filterErrorSelected
        default:
            return false
        }
    }
}

struct TableEntry: View {
    let log: LogEntry
    let keywords: [String]

    var body: some View {
        HStack {
            Text("\(log.ts.dropLast(5))").padding(.trailing, 5)
            Text("\(log.L)").frame(width: CGFloat(50), alignment: .leading).foregroundColor(getLogLevelColor())
            Text("\(log.C)").frame(width: CGFloat(180), alignment: .leading).foregroundColor(.gray).padding(.trailing, 5).textSelection(.enabled)
            HighlightedText(log: log.M, keywords: keywords).padding(.trailing, 3)
        }
        .font(.system(.body, design: .monospaced))
        .frame(minWidth: .leastNonzeroMagnitude)
        .textSelection(.enabled)
    }

    func getLogLevelColor() -> Color {
        switch log.L {
        case LogLevel.debug.rawValue:
            return Color.purple
        case LogLevel.info.rawValue:
            return Color.accentColor
        case LogLevel.warn.rawValue:
            return Color.yellow
        default:
            return Color.red
        }
    }
}

struct HighlightedText: View {
    var log: String
    var keywords: [String]

    var body: some View {
        let attributedString = NSMutableAttributedString(string: log)
        let range = NSRange(location: 0, length: log.utf16.count)

        for keyword in keywords {
            do {
                let regex = try NSRegularExpression(pattern: keyword, options: .caseInsensitive)
                regex.enumerateMatches(in: log, options: [], range: range) { match, _, _ in
                    let matchRange = match!.range
                    attributedString.addAttribute(.foregroundColor, value: NSColor.accent, range: matchRange)
                }
            } catch {
                print("error highlighting keywords: \(error)")
            }
        }

        return AttributedText(attributedString: attributedString)
    }
}

struct AttributedText: NSViewRepresentable {
    var attributedString: NSAttributedString

    func makeNSView(context _: Context) -> NSTextField {
        let textField = NSTextField(labelWithAttributedString: attributedString)
        textField.isEditable = false
        textField.isSelectable = true
        textField.isBordered = false
        textField.backgroundColor = .clear
        textField.font = NSFont.monospacedSystemFont(ofSize: NSFont.systemFontSize, weight: .regular)
        return textField
    }

    func updateNSView(_ nsView: NSTextField, context _: Context) {
        nsView.attributedStringValue = attributedString
    }
}

struct ContentView_Previews: PreviewProvider {
    static let mock = MockLogsService()
    static var previews: some View {
        ContentView(logsService: mock, sideBarVisibility: .automatic)
    }
}
