@testable import GlassdoorUI
import XCTest

class StringExtensionTests: XCTestCase {
    func testRemoveSuffix() {
        let string = "Hello, World!"
        let expectedOutput = "Hello, World"
        let output = string.removeSuffix("!")
        XCTAssertEqual(output, expectedOutput, "Expected '\(expectedOutput)' but got '\(output)'")
    }

    func testConvertToArray() {
        let string = "keyword1,keyword2, keyword3"
        let expectedOutput = ["keyword1", "keyword2", "keyword3"]
        let output = string.convertToArray()
        XCTAssertEqual(output, expectedOutput, "Expected \(expectedOutput) but got \(output)")
    }
}
