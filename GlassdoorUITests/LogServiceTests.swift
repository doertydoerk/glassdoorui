@testable import GlassdoorUI
import XCTest

var service = LogsService()

class LogServiceTests: XCTestCase {
    func testMatchesLocalhost() {
        XCTAssertTrue(service.matchesHostnamePattern("localhost"))
        XCTAssertFalse(service.matchesHostnamePattern("localhost:4200"))
    }

    func testMatchesValidFxtLocal() {
        XCTAssertTrue(service.matchesHostnamePattern("fxt-1234567.local"))
    }

    func testDoesNotMatchInvalidFxtLocal() {
        XCTAssertFalse(service.matchesHostnamePattern("fxt-abcdefg.local"))
    }

    func testDoesNotMatchOtherHostnames() {
        XCTAssertFalse(service.matchesHostnamePattern("example.com"))
        XCTAssertFalse(service.matchesHostnamePattern("fxt-1234567"))
        XCTAssertFalse(service.matchesHostnamePattern("fxt-1234567.localdomain"))
    }
}
