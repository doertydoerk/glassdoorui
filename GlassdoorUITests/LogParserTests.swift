@testable import GlassdoorUI
import XCTest

final class LogParserTests: XCTestCase {
    let parser = LogParser()

    func testParseFile() throws {
        let basePath = Bundle(for: LogParserTests.self)
        let filPath = "file://\(basePath.bundlePath)/Contents/Resources/test.log"
        let actual = parser.parserFile(filPath)
        XCTAssertEqual(325, actual.count)
    }
}
