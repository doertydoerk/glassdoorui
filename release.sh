#!/bin/bash

######################################################################
# Set version tag in format vX.Y.Z before running this script.
# npm run release is disabled for now so no changelog will be created.
######################################################################

BRANCH="main"
TARGET_NAME="GlassdoorUI"
EXPORT_PLIST_PATH="./GlassdoorUI/exportOptions.plist"
PROJECT_FILE="./GlassdoorUI.xcodeproj/project.pbxproj"
TEAM_ID=${MAC_APP_TEAM_ID}

VERSION=$1
BUILD=$(git rev-list --count "${BRANCH}")

xcrun agvtool new-marketing-version "${VERSION}"
xcrun agvtool new-version "${BUILD}"

# create archive
xcodebuild archive -scheme GlassdoorUI -archivePath ./release/"${VERSION}"/archive/"${TARGET_NAME}".xcarchive

# app from archive
xcodebuild -exportArchive -archivePath ./release/"${VERSION}"/archive/"${TARGET_NAME}".xcarchive -exportPath ./release/"${VERSION}"/app -exportOptionsPlist "${EXPORT_PLIST_PATH}"
codesign -f -o runtime --timestamp -s "$TEAM_ID" ./release/"${VERSION}"/app/"${TARGET_NAME}".app

# create DMG
# https://github.com/sindresorhus/create-dmg
create-dmg ./release/"${VERSION}"/app/"${TARGET_NAME}".app ./release/"${VERSION}"
mv "./release/${VERSION}/${TARGET_NAME} 1.0.dmg" "./release/${VERSION}/${TARGET_NAME} ${VERSION}".dmg

# notarize - https://blog.rampatra.com/how-to-notarize-a-dmg-or-zip-file-with-the-help-of-xcode-s-notary-tool
# notarization requires a paid Aple Developer membership
#xcrun notarytool submit "./release/${VERSION}/${TARGET_NAME} ${VERSION}.dmg" --keychain-profile "GlassdoorUI" --wait

# staple
xcrun stapler staple "./release/$VERSION/GlassdorUI ${VERSION}.dmg"

# clean up
rm -rf "./release/${VERSION}/a*"

# git push everything including tags, artifacts, changelog, ...
git add .
git commit --amend --no-edit
git tag "${VERSION}"
git push origin "${BRANCH}" --tags
